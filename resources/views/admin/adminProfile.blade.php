<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Staff Movement System</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">

  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <link rel="stylesheet" href="{{ asset('assets/vendor/aos/aos.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap/css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap-icons/bootstrap-icons.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/boxicons/css/boxicons.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/glightbox/css/glightbox.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/swiper/swiper-bundle.min.css') }}">

  <link rel="stylesheet" href="{{ asset('assets/css/Profile.css') }}">
</head>

<body>
  <i class="bi bi-list mobile-nav-toggle d-xl-none"></i>
  <header id="header">
    <div class="d-flex flex-column">
      <div class="profile">
        <img src="{{ asset('assets/img/logo.png') }}" alt="No Logo" class="img-fluid">
        <h1 class="text-light"><a href="index.html"></a></h1>
      </div>
      <nav id="navbar" class="nav-menu navbar">
        <ul>
            <li><a href="{{ url('/admin/dashboard') }}" class="nav-link scrollto"><i class="fas fa-home"></i> <span>DashBoard</span></a></li>
            <li><a href="{{ url('/admin/viewDoctor') }}" class="nav-link scrollto"><i class="fas fa-user-md"></i> <span>Doctor</span></a></li>
            <li><a href="{{ url('/admin/viewHospital') }}" class="nav-link scrollto"><i class="fas fa-hospital"></i> <span>Hospitals</span></a></li>
            <li><a href="{{ url('/admin/viewDirector') }}" class="nav-link scrollto"><i class="fas fa-user-circle"></i> <span>Director</span></a></li>
            <li><a href="{{ url('/admin/viewAdm') }}" class="nav-link scrollto "><i class="fas fa-user"></i> <span>Adm</span></a></li>
            <li><a href="{{ url('/admin/adminProfile') }}" class="nav-link scrollto active"><i class="fas fa-user-circle"></i> <span>Profile</span></a></li>
            <li>
                <form method="POST" action="{{ route('logout') }}">
                    @csrf
                    <a href="#"  class="nav-link scrollto" onclick="event.preventDefault();this.closest('form').submit();"><i class="fas fa-sign-out-alt"></i><span>Log Out</span></a>
                </form>
            </li>
        </ul>
      </nav>
    </div>
  </header>
  <main id="main">
    <section id="hero" class="about">
      <div class="container">
        <h4 style="font-weight:800;">Profile DashBoard</h4>
        <div class="row justify-content-center">
          <div class="col-md-6" style="display: flex; flex-direction: column;margin-top: 20px;">
              <div class="image-container" style="margin: auto;">
                  <img src="{{ asset('/storage/images/' . $user->image) }}" alt="{{ $user->name }}" class="img-fluid">
              </div>
              <div>
                <div class=" mb-5 menu-container">
                    <div class="menu-toggle">
                        <i class="fas fa-ellipsis-h"></i>
                    </div>
                    <div class="menu-items">
                        <div class="menu-item">
                            <i class="fas fa-edit"></i>
                            <a href="{{ route('adminEditProfile', ['cid' => $user->cid]) }}" title="Edit Profile">Edit Profile</a>
                        </div>
                        <div class="menu-item">
                            <i class="fas fa-lock"></i>
                            <a href="{{ route('adminResetPassword', ['cid' => $user->cid]) }}" title="Reset Password">Reset Password</a>
                        </div>
                    </div>
                </div>

                <form class="col-lg-12" style="margin-top: 50px">
                    <div class="col-lg-10 m-auto ">
                        <label class="col-lg-8 mb-2" for="exampleInputEmail1" style="margin-right:20px">Name : {{ $user->name }}</label>
                    </div>

                    <div class="col-lg-10 my-2 m-auto ">
                        <label class="col-lg-8 mb-2" for="exampleInputEmail1" style="margin-right:20px">Email : {{ $user->email }}</label>
                    </div>

                    <div class="col-lg-10 m-auto ">
                        <label class="col-lg-8 mb-2" for="exampleInputEmail1" style="margin-right:20px">Contact : {{ $user->phone }}</label>
                    </div>
                </form>
              </div>
          </div>
      </div>
      </div>
    </section>
  </main>
  <script src="{{ asset('assets/js/main.js') }}"></script>
  <script>
    document.addEventListener('DOMContentLoaded', function() {
    var menuToggle = document.querySelector('.menu-toggle');
    var menuItems = document.querySelector('.menu-items');

    menuToggle.addEventListener('click', function() {
        menuItems.style.display = menuItems.style.display === 'block' ? 'none' : 'block';
    });

    document.addEventListener('click', function(event) {
        if (!menuToggle.contains(event.target) && !menuItems.contains(event.target)) {
            menuItems.style.display = 'none';
        }
    });
});

  </script>
</body>

</html>
