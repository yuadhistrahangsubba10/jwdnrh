<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Staff Movement System</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">

  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <link rel="stylesheet" href="{{ asset('assets/vendor/aos/aos.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap/css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap-icons/bootstrap-icons.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/boxicons/css/boxicons.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/glightbox/css/glightbox.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/swiper/swiper-bundle.min.css') }}">

  <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
</head>
<body>

  <i class="bi bi-list mobile-nav-toggle d-xl-none"></i>

  <header id="header">
    <div class="d-flex flex-column">

      <div class="profile">
        <img src="{{ asset('assets/img/logo.png') }}" alt="No Logo" class="img-fluid">
        <h1 class="text-light"><a href="index.html"></a></h1>
      </div>

      <nav id="navbar" class="nav-menu navbar">
        <ul>
          <li><a href="{{ url('/admin/dashboard') }}" class="nav-link scrollto  active"><i class="fas fa-home"></i> <span>DashBoard</span></a></li>
          <li><a href="{{ url('/admin/viewDoctor') }}" class="nav-link scrollto"><i class="fas fa-user-md"></i> <span>Doctor</span></a></li>
          <li><a href="{{ url('/admin/viewHospital') }}" class="nav-link scrollto"><i class="fas fa-hospital"></i> <span>Hospitals</span></a></li>
          <li><a href="{{ url('/admin/viewDirector') }}" class="nav-link scrollto"><i class="fas fa-user-circle"></i> <span>Director</span></a></li>
          <li><a href="{{ url('/admin/viewAdm') }}" class="nav-link scrollto "><i class="fas fa-user"></i> <span>Adm</span></a></li>
          <li><a href="{{ url('/admin/adminProfile') }}" class="nav-link scrollto"><i class="fas fa-user-circle"></i> <span>Profile</span></a></li>
          <li>
            <form method="POST" action="{{ route('logout') }}">
                @csrf
                <a href="#"  class="nav-link scrollto" onclick="event.preventDefault();this.closest('form').submit();"><i class="fas fa-sign-out-alt"></i><span>Log Out</span></a>
            </form>
        </li>
      </ul>
      </nav>
    </div>
  </header>

<main id="main">
    <section id="hero" class="about">
        <div class="container text-left">
            <h4 style="font-weight:800;">Admin DashBoard</h4>

            <div class="row">
                <div class="column">
                    <div class="box" style="background-image: url('{{ asset('assets/img/hospital.jpeg') }}');">
                        <h5>Total Doctor</h5>
                        <p><?php echo App\Models\Doctors::count(); ?></p>
                    </div>
                </div>
                <div class="column">
                    <div class="box" style="background-image:  url('{{ asset('assets/img/hospital.jpeg') }}');">
                        <h5>Total Director</h5>
                        <p><?php echo App\Models\User::where('role', 'director')->count(); ?></p>
                    </div>
                </div>
                <div class="column">
                    <div class="box" style="background-image:  url('{{ asset('assets/img/hospital.jpeg') }}');">
                        <h5>Total Adm</h5>
                        <p><?php echo App\Models\User::where('role', 'adm')->count(); ?></p>
                    </div>
                </div>
                <div class="column">
                    <div class="box" style="background-image:  url('{{ asset('assets/img/hospital.jpeg') }}');">
                        <h5>Total Hospital</h5>
                        <p><?php echo App\Models\Hospitals::count(); ?></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8">
            <h6 class="ms-4 mb-2" style="font-weight: bold;">Monthly Analysis</h6>
                <div class="container-fluid" style="margin-left: 5px; box-shadow: rgba(0, 0, 0, 0.1) 0px 4px 6px -1px, rgba(0, 0, 0, 0.06) 0px 2px 4px -1px; height:57vh;">
                    <canvas class='w-100'  id="myChart"></canvas>
                </div>
            </div>
            <div class="col-lg-3 m-0 p-0 d-flex flex-column me-5 ms-0 " >
                <h6 class="mb-2" style="font-weight: bold;">Recent Activities</h6>
                    <div class="activity-container" style="height: 57vh; overflow-y: auto;">
                    @foreach($recent->sortByDesc('created_at') as $r)
                        @php
                            $timestamp = \Illuminate\Support\Carbon::parse($r->created_at);
                            $currentTime = \Illuminate\Support\Carbon::now();
                            $timeDifference = $timestamp->diffForHumans($currentTime);
                        @endphp
                        <a href="{{ route('adminViewDoctor', ['cid' => $r->cid]) }}" class="text-decoration-none text-dark">
                            <div class="w-100 position-relative gap-3 mt-2 ps-3 py-2 d-flex flex-row rounded-2 align-items-center" style="box-shadow: rgba(0, 0, 0, 0.1) 0px 4px 6px -1px, rgba(0, 0, 0, 0.06) 0px 2px 4px -1px; border: 1px solid #0000001f;height: 80px;">
                                <?php
                                    $user = \App\Models\Doctors::where('cid', $r->cid)->first();

                                    if ($user) {
                                        echo '<img style="object-fit: cover; object-position: center;" width="50" height="50" class="rounded-5" src="' . asset('/storage/images/' . $user->image) . '" alt="' . $user->name . '" alt="profile-image">';
                                    } else {
                                        echo 'User not found';
                                    }
                                ?>
                                <p class="mt-2 mb-2">
                                    <span style="font-size: 15px;font-weight: 700;">Dr. {{ $r->name }}</span> <br>
                                    <span style="font-size: 14px;">{{ $r->hospitalName }}</span>
                                </p>
                                <p class="p-0 m-0 position-absolute" style="right: 5px; bottom: 4px; font-size: 11px;">{{ $timeDifference }}</p>
                            </div>
                        </a>
                    @endforeach
                    </div>
            </div>
        </div>
        <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-scrollable modal-lg modal-dialog-centered" style="margin-left: 400px;">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" id="downloadCSV">Download</button>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>

<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
<script src="{{ asset('assets/js/main.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script>

<script>
    var chartData = @json($data);
    var colorMapping = {
        'On Leave': 'rgba(255, 99, 132, 1)',
        'On Tour': 'rgba(75, 192, 192, 1)',
        'On Event': 'rgba(54, 162, 235, 1)'
    };
    var labels = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    var datasets = {};

    chartData.forEach(item => {
        if (!datasets[item.type]) {
            datasets[item.type] = Array(12).fill(0);
        }
        datasets[item.type][item.month - 1] = item.count;
    });

    function showPopup(title, doctorHospitalPairs) {
    $('#exampleModalLabel').text(title);
    var popupContent = '<div class="list-group">';
    doctorHospitalPairs.forEach(function(pair) {
        popupContent += '<a href="/admin/viewDoctorDetail/' + pair.cid + '" class="list-group-item list-group-item-action" data-cid="' + pair.cid + '">';
        popupContent += '<div class="w-100 position-relative gap-3 ps-3 py-2 d-flex flex-row rounded-2 align-items-center" style="box-shadow: rgba(0, 0, 0, 0.1) 0px 4px 6px -1px, rgba(0, 0, 0, 0.06) 0px 2px 4px -1px;border: 1px solid #0000001f;height: 80px;">';
        popupContent += '<img style="object-fit: cover; object-position: center;" width="50" height="50" class="rounded-circle" src="https://images.pexels.com/photos/428364/pexels-photo-428364.jpeg?cs=srgb&dl=pexels-spencer-selover-428364.jpg&fm=jpg" alt="profile-image">';
        popupContent += '<p class="mt-2 mb-2">';
        popupContent += '<span style="font-size: 15px;font-weight: 700;">Dr. ' + pair.doctorName + '</span> <br>';
        popupContent += '<span style="font-size: 14px;">' + pair.hospitalName + '</span>';
        popupContent += '</p></div></a>';
    });
    popupContent += '</div>';
    $('.modal-body').html(popupContent);
    $('#exampleModal').modal('show');

    $('.list-group-item').on('click', function(event) {
    event.preventDefault(); // Prevent default link behavior (page reload)
    var href = $(this).attr('href'); // Get the href attribute of the clicked link
    if (href && href !== "#") {
        var cid = href.split('/').pop(); // Extract the cid from the href attribute
        if (cid !== "undefined") {
            window.location.href = '/admin/viewDoctorDetail/' + cid; // Redirect to the admin viewDoctorDetail page with the cid
        } else {
            console.error("CID is undefined in link:", href);
        }
    } else {
        console.error("Href attribute is not set or is empty.");
    }
});
}

    function getElementsAtEvent(chart, event) {
        var eventPosition = Chart.helpers.getRelativePosition(event, chart);
        return chart.getElementsAtEventForMode(eventPosition, 'nearest', { intersect: true }, false);
    }

    var ctx = document.getElementById('myChart').getContext('2d');

    if (window.myChart) {
        window.myChart.destroy();
    }

    var myChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: labels,
            datasets: Object.keys(datasets).map(key => ({
                label: key,
                data: datasets[key],
                borderColor: colorMapping[key],
                backgroundColor: colorMapping[key],
                borderWidth: 1,
                fill: false
            }))
        },
        options: {
            scales: {
                x: {
                    grid: {
                    display: false
                    },
                    title: {
                        display: true,
                        text: 'Months'
                    }
                },
                y: {
                    // grid: {
                    //     display: false
                    // },
                    title: {
                        display: true,
                        text: 'Number'
                    },
                    ticks: {
                        beginAtZero: true,
                        stepSize: 1
                    }
                }
            },
            onClick: function(event) {
                var elements = myChart.getElementsAtEventForMode(event.native, 'nearest', { intersect: true }, false);
                var activePoint = elements[0];
                if (activePoint) {
                    var monthIndex = activePoint.index;
                    var leaveType = myChart.data.datasets[activePoint.datasetIndex].label;

                    $.ajax({
                        url: '/admin/dashboard/getDoctorsOnLeave',
                        method: 'GET',
                        data: { month: monthIndex + 1, leaveType: leaveType },
                        success: function(response) {
                            // console.log("Response from server:", response);
                            var title = "Doctors " + leaveType + " in " + labels[monthIndex];
                            var doctorNames = response.map(function(item) {
                                return {
                                    doctorName: item.name,
                                    hospitalName: item.hospitalName,
                                    cid: item.cid
                                };
                            });
                            showPopup(title, doctorNames);
                        },
                        error: function(xhr, status, error) {
                            console.error("Error:", error);
                        }
                    });


                }
            }
        }
    });
    $('#downloadCSV').on('click', function() {
    var title = $('#exampleModalLabel').text();
    var doctorHospitalPairs = $('#exampleModal .list-group-item').map(function() {
        var doctorName = $(this).find('.mt-2 > span:first-child').text().replace('Dr. ', '');
        var hospitalName = $(this).find('.mt-2 > span:nth-child(3)').text();
        var cid = $(this).data('cid');
        return { doctorName: doctorName, hospitalName: hospitalName, cid: cid };
    }).get();
    downloadCSV(title, doctorHospitalPairs);
});

    function downloadCSV(title, doctorHospitalPairs) {
        var csvContent = "data:text/csv;charset=utf-8,";
        csvContent += "Title: " + title + "\r\n";
        csvContent += "Name,HospitalName,CID\r\n";
        doctorHospitalPairs.forEach(function(pair) {
            csvContent += pair.doctorName + "," + pair.hospitalName + "," + pair.cid + "\r\n";
        });

        var encodedUri = encodeURI(csvContent);
        var link = document.createElement("a");
        link.setAttribute("href", encodedUri);
        link.setAttribute("download", title + ".csv");
        document.body.appendChild(link);
        link.click();
    }
</script>
</body>
</html>
