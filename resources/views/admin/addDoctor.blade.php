{{-- <!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Staff Movement System</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <!-- Vendor CSS Files -->
  <link rel="stylesheet" href="{{ asset('assets/vendor/aos/aos.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap/css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap-icons/bootstrap-icons.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/boxicons/css/boxicons.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/glightbox/css/glightbox.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/swiper/swiper-bundle.min.css') }}">

  <!-- Template Main CSS File -->
  <link rel="stylesheet" href="{{ asset('assets/css/Adduser.css') }}">
</head>
<body>
  <!-- ======= Mobile nav toggle button ======= -->
  <i class="bi bi-list mobile-nav-toggle d-xl-none"></i>
  <!-- ======= Header ======= -->
  <header id="header">
    <div class="d-flex flex-column">
      <div class="profile">
        <img src="{{ asset('assets/img/logo.png') }}" alt="No Logo" class="img-fluid">
        <h1 class="text-light"><a href="index.html"></a></h1>
      </div>
      <nav id="navbar" class="nav-menu navbar">
        <ul>
            <li><a href="{{ url('/admin/dashboard') }}" class="nav-link scrollto"><i class="fas fa-home"></i> <span>DashBoard</span></a></li>
            <li><a href="{{ url('/admin/viewDoctor') }}" class="nav-link scrollto  active"><i class="fas fa-user-md"></i> <span>Doctor</span></a></li>
            <li><a href="{{ url('/admin/viewHospital') }}" class="nav-link scrollto"><i class="fas fa-hospital"></i> <span>Hospitals</span></a></li>
            <li><a href="{{ url('/admin/viewDirector') }}" class="nav-link scrollto"><i class="fas fa-user-circle"></i> <span>Director</span></a></li>
            <li><a href="{{ url('/admin/viewAdm') }}" class="nav-link scrollto "><i class="fas fa-user"></i> <span>Adm</span></a></li>
            <li><a href="{{ url('/admin/adminProfile') }}" class="nav-link scrollto"><i class="fas fa-user-circle"></i> <span>Profile</span></a></li>
            <li>
                <form method="POST" action="{{ route('logout') }}">
                    @csrf
                    <a href="#"  class="nav-link scrollto" onclick="event.preventDefault();this.closest('form').submit();"><i class="fas fa-sign-out-alt"></i><span>Log Out</span></a>
                </form>
            </li>
        </ul>
      </nav>
    </div>
  </header>



  <main id="main">
 <section id="hero" class="about">
  <div class="container text-left">
    <h4 style="font-weight:800; margin: 50px 0px;">Add Doctor</h4>
                    @if(session('success'))
                        <div class="alert alert-success" id="successAlert">
                            {{ session('success') }}
                        </div>
                        <script>
                            setTimeout(function() {
                                $('#successAlert').fadeOut('fast');
                            }, 5000);
                        </script>
                    @endif

                    @if(session('error'))
                        <div id="error-alert" class="alert alert-danger">
                            {{ session('error') }}
                        </div>
                        <script>
                            setTimeout(function() {
                                var errorAlert = document.getElementById('error-alert');
                                if(errorAlert) {
                                    errorAlert.style.display = 'none';
                                }
                            }, 5000);
                        </script>
                    @endif

      <form id="form" method="POST" action="{{ route('addDoctor') }}"  enctype="multipart/form-data">
        {!! csrf_field() !!}
        <div class="form-row">
            <div class="col-lg-4 mb-lg-4 mr-lg-4 mb-3">
                <label for="exampleInputEmail1">Name</label>
                <input type="text" name="name" id="name" class="form-control rounded-1" >
                <div id="nameError" class="error-message" style="color: red; font-size: 14px;"></div>

            </div>

            <div class="col-lg-4 mb-lg-4 ml-lg-4 mb-3">
                <label for="exampleInputEmail1">Email address</label>
                <input type="text" name="email" id="email" class="form-control rounded-1" >
                <div id="emailError" class="error-message" style="color: red; font-size: 14px;"></div>

            </div>
        </div>
        <div class="form-row">
            <div class="col-lg-4 mb-lg-4 mr-lg-4 mb-3">
                <label for="exampleInputEmail1">Phone Number</label>
                <input type="text" name="phone" id="phone" class="form-control rounded-1" >
                <div id="phoneError" class="error-message" style="color: red; font-size: 14px;"></div>

            </div>

            <div class="col-lg-4 mb-lg-4 ml-lg-4 mb-3">
                <label for="exampleInputEmail1">CID Number</label>
                <input type="text" name="cid" id="cid" class="form-control rounded-1" >
                            <div id="cidError" class="error-message" style="color: red; font-size: 14px;"></div>

            </div>
        </div>
        <div class="form-row">
            <div class="col-lg-4 mr-lg-4 mr-lg-4 mb-lg-4 mb-3">
                <label for="exampleInputEmail1">Specialization</label>
                <input type="text" name="specialization" id="specialization" class="form-control rounded-1" >
            </div>
            <div class="col-lg-4 mb-lg-4 ml-lg-4 mb-3">
                <label for="gender">Gender</label>
                <select name="gender" id="gender" class="form-control rounded-1">
                    <option value=""></option>
                    <option value="male">Male</option>
                    <option value="female">Female</option>
                </select>
            </div>
        </div>

        <div class="form-row">
            <div class="col-lg-4 mb-lg-4 mr-lg-4 mb-3">
                <label for="location">Location</label>
                <select name="location" id="location" class="form-control rounded-1">
                    <option value=""></option>

                </select>
            </div>

            <div class="col-lg-4 mb-lg-4 ml-lg-4 mb-3">
                <label for="hospitalName">Hospital</label>
                <select name="hospitalName" id="hospitalName" class="form-control rounded-1">
                    <option value=""></option>

                </select>
            </div>
        </div>

        <div class="form-row">
            <div class="col-12 text-area">
            <label>background Details</label>
            <textarea class="form-control rounded-1" name="description" id="description" rows="5"></textarea>
            </div>
        </div>
        <div id="error" style="font-size:14px;font-weight:600;color:red;margin-top:15px"></div>
        <input type="file" name="image" id="image" class="form-control col-lg-3 mt-4"  style="font-size: 14px;"/>

        <div class="d-md-flex justify-content-start align-items-center mt-4" style="margin-bottom: 40px;">
            <button class="btn btn-primary " type="submit" value="Submit">Save</button>
            <button class="btn-2 " type="button">Cancel</button>
        </div>
    </form>

    </div>
</section>
</main>
<script src="{{ asset('assets/js/main.js') }}"></script>
<script>
    const form = document.getElementById('form');
    const name = document.getElementById('name');
    const email = document.getElementById('email');
    const phone = document.getElementById('phone');
    const cid = document.getElementById('cid');

    const nameError = document.getElementById('nameError');
    const emailError = document.getElementById('emailError');
    const phoneError = document.getElementById('phoneError');
    const cidError = document.getElementById('cidError');

const errorElement = document.getElementById('error');

form.addEventListener('submit', (e) => {
  let messages = [];

  if (name.value.trim() === '') {
    messages.push('* Name is required');
    displayErrors(messages);
    e.preventDefault();
    return;
  }
  if (!email.value.match(/^\S+@\S+\.\S+$/)) {
    messages.push('* Email is not valid');
    displayErrors(messages);
    e.preventDefault();
    return;
  }
  if (phone.value.trim() === '') {
    messages.push('* Phone Number is required');
    displayErrors(messages);
    e.preventDefault();
    return;
  }
  if (!phone.value.match(/^(17|77)\d{6}$/) || phone.value.length !== 8) {
    messages.push('* invalid Phone Number');
    displayErrors(messages);
    e.preventDefault();
    return;
  }
  if (cid.value.trim() === '') {
    messages.push('* cid is required');
    displayErrors(messages);
    e.preventDefault();
    return;
  }
  if (cid.value.length !== 11) {
    messages.push('* Cid number should be 11 digits long');
    displayErrors(messages);
    e.preventDefault();
    return;
  }

});
function displayErrors(messages) {
  errorElement.innerText = messages[0];
}
 </script>
</body>
</html> --}}
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Staff Movement System</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">

  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

  <link rel="stylesheet" href="{{ asset('assets/vendor/aos/aos.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap/css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap-icons/bootstrap-icons.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/boxicons/css/boxicons.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/glightbox/css/glightbox.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/swiper/swiper-bundle.min.css') }}">

  <link rel="stylesheet" href="{{ asset('assets/css/Adduser.css') }}">
</head>
<body>
  <i class="bi bi-list mobile-nav-toggle d-xl-none"></i>
  <header id="header">
    <div class="d-flex flex-column">
      <div class="profile">
        <img src="{{ asset('assets/img/logo.png') }}" alt="No Logo" class="img-fluid">
        <h1 class="text-light"><a href="index.html"></a></h1>
      </div>
      <nav id="navbar" class="nav-menu navbar">
        <ul>
            <li><a href="{{ url('/admin/dashboard') }}" class="nav-link scrollto"><i class="fas fa-home"></i> <span>DashBoard</span></a></li>
            <li><a href="{{ url('/admin/viewDoctor') }}" class="nav-link scrollto  active"><i class="fas fa-user-md"></i> <span>Doctor</span></a></li>
            <li><a href="{{ url('/admin/viewHospital') }}" class="nav-link scrollto"><i class="fas fa-hospital"></i> <span>Hospitals</span></a></li>
            <li><a href="{{ url('/admin/viewDirector') }}" class="nav-link scrollto"><i class="fas fa-user-circle"></i> <span>Director</span></a></li>
            <li><a href="{{ url('/admin/viewAdm') }}" class="nav-link scrollto "><i class="fas fa-user"></i> <span>Adm</span></a></li>
            <li><a href="{{ url('/admin/adminProfile') }}" class="nav-link scrollto"><i class="fas fa-user-circle"></i> <span>Profile</span></a></li>
            <li>
                <form method="POST" action="{{ route('logout') }}">
                    @csrf
                    <a href="#"  class="nav-link scrollto" onclick="event.preventDefault();this.closest('form').submit();"><i class="fas fa-sign-out-alt"></i><span>Log Out</span></a>
                </form>
            </li>
        </ul>
      </nav>
    </div>
  </header>

  <main id="main">
    <section id="hero" class="about">
      <div class="container text-left">
        <h4 style="font-weight:800; margin: 50px 0px;">Add Doctor</h4>
        @if(session('success'))
            <div class="alert alert-success" id="successAlert">
                {{ session('success') }}
            </div>
            <script>
                setTimeout(function() {
                    $('#successAlert').fadeOut('fast');
                }, 5000);
            </script>
        @endif

        @if(session('error'))
            <div id="error-alert" class="alert alert-danger">
                {{ session('error') }}
            </div>
            <script>
                setTimeout(function() {
                    var errorAlert = document.getElementById('error-alert');
                    if(errorAlert) {
                        errorAlert.style.display = 'none';
                    }
                }, 5000);
            </script>
        @endif

        <form id="form" method="POST" action="{{ route('addDoctor') }}" enctype="multipart/form-data">
          {!! csrf_field() !!}
          <div class="form-row">
              <div class="col-lg-4 mb-lg-4 mr-lg-4 mb-3">
                  <label for="exampleInputEmail1">Name</label>
                  <input type="text" name="name" id="name" class="form-control rounded-1" >
                  <div id="nameError" class="error-message" style="color: rgb(230, 13, 13);font-size:13px;font-weight:600;margin-top:5px"></div>
              </div>
              <div class="col-lg-4 mb-lg-4 ml-lg-4 mb-3">
                  <label for="exampleInputEmail1">Email address</label>
                  <input type="text" name="email" id="email" class="form-control rounded-1" >
                  <div id="emailError" class="error-message" style="color: rgb(230, 13, 13);font-size:13px;font-weight:600;margin-top:5px"></div>
              </div>
          </div>
          <div class="form-row">
              <div class="col-lg-4 mb-lg-4 mr-lg-4 mb-3">
                  <label for="exampleInputEmail1">Phone Number</label>
                  <input type="text" name="phone" id="phone" class="form-control rounded-1" >
                  <div id="phoneError" class="error-message" style="color: rgb(230, 13, 13);font-size:13px;font-weight:600;margin-top:5px"></div>
              </div>
              <div class="col-lg-4 mb-lg-4 ml-lg-4 mb-3">
                  <label for="exampleInputEmail1">CID Number</label>
                  <input type="text" name="cid" id="cid" class="form-control rounded-1" >
                  <div id="cidError" class="error-message" style="color: rgb(230, 13, 13);font-size:13px;font-weight:600;margin-top:5px"></div>
              </div>
          </div>
          <div class="form-row">
              <div class="col-lg-4 mr-lg-4 mr-lg-4 mb-lg-4 mb-3">
                  <label for="exampleInputEmail1">Specialization</label>
                  <input type="text" name="specialization" id="specialization" class="form-control rounded-1" >
                  <div id="spError" class="error-message" style="color: rgb(230, 13, 13);font-size:13px;font-weight:600;margin-top:5px"></div>
              </div>
              <div class="col-lg-4 mb-lg-4 ml-lg-4 mb-3">
                  <label for="gender">Gender</label>
                  <select name="gender" id="gender" class="form-control rounded-1">
                      <option value=""></option>
                      <option value="male">Male</option>
                      <option value="female">Female</option>
                  </select>
                  <div id="genderError" class="error-message" style="color: rgb(230, 13, 13);font-size:13px;font-weight:600;margin-top:5px"></div>
              </div>
          </div>
          <div class="form-row">
              <div class="col-lg-4 mb-lg-4 mr-lg-4 mb-3">
                <label for="location">Location</label>
                    <select name="location" id="location" class="form-control rounded-1">
                        <option value=""></option>
                        <?php
                        $locations = App\Models\Hospitals::pluck('location')->unique();
                        if($locations->count() > 0) {
                            foreach($locations as $location) {
                                echo '<option value="' . $location . '">' . $location . '</option>';
                            }
                        } else {
                            echo '<option disabled>No locations available</option>';
                        }
                        ?>
                    </select>
                    <div id="locationError" class="error-message" style="color: rgb(230, 13, 13);font-size:13px;font-weight:600;margin-top:5px"></div>
              </div>
              <div class="col-lg-4 mb-lg-4 ml-lg-4 mb-3">
                  <label for="hospitalName">Hospital</label>
                  <select name="hospitalName" id="hospitalName" class="form-control rounded-1">
                    <option value=""></option>
                    <?php
                    $hospitals = App\Models\Hospitals::pluck('name');
                    if($hospitals->count() > 0) {
                        foreach($hospitals as $hospital) {
                            echo '<option value="' . $hospital . '">' . $hospital . '</option>';
                        }
                    } else {
                        echo '<option disabled>No hospitals available</option>';
                    }
                    ?>
                </select>
                  <div id="hospitalError" class="error-message" style="color: rgb(230, 13, 13);font-size:13px;font-weight:600;margin-top:5px"></div>
              </div>
          </div>
          <div class="form-row">
              <div class="col-12 text-area">
                <label>Background Details</label>
                <textarea class="form-control rounded-1" name="description" id="description" rows="5"></textarea>
              </div>
          </div>
          <input type="file" name="image" id="image" class="form-control col-lg-3 mt-4" style="font-size: 14px;"/>
          <div class="d-md-flex justify-content-start align-items-center mt-4" style="margin-bottom: 40px;">
            <button class="btn btn-primary " type="submit" value="Submit">Save</button>
            <button class="btn-2 " id="cancelButton" type="button">Cancel</button>
        </div>
        </form>
      </div>
    </section>
  </main>

  <script>
    document.getElementById('form').addEventListener('submit', function(event) {
        // Get form field values
        const name = document.getElementById('name').value.trim();
        const email = document.getElementById('email').value.trim();
        const phone = document.getElementById('phone').value.trim();
        const cid = document.getElementById('cid').value.trim();
        const specialization = document.getElementById('specialization').value.trim();
        const gender = document.getElementById('gender').value.trim();
        const location = document.getElementById('location').value.trim();
        const hospitalName = document.getElementById('hospitalName').value.trim();

        // Validate form fields
        let isValid = true;

        // Validate name
        if (!name) {
            document.getElementById('nameError').textContent = "Name is required";
            isValid = false;
        } else {
            document.getElementById('nameError').textContent = "";
        }

        // Validate email
        if (!email) {
            document.getElementById('emailError').textContent = "Email is required";
            isValid = false;
        } else if (!validateEmail(email)) {
            document.getElementById('emailError').textContent = "Invalid email format";
            isValid = false;
        } else {
            document.getElementById('emailError').textContent = "";
        }

        // Validate phone
        if (!phone) {
            document.getElementById('phoneError').textContent = "Phone number is required";
            isValid = false;
        } else if (!validatePhoneNumber(phone)) {
            document.getElementById('phoneError').textContent = "Invalid phone number format";
            isValid = false;
        } else {
            document.getElementById('phoneError').textContent = "";
        }

        // Validate CID
        if (!cid) {
            document.getElementById('cidError').textContent = "CID number is required";
            isValid = false;
        } else {
            document.getElementById('cidError').textContent = "";
        }

        // Validate specialization
        if (!specialization) {
            document.getElementById('spError').textContent = "Specialization is required";
            isValid = false;
        } else {
            document.getElementById('spError').textContent = "";
        }

        // Validate gender
        if (!gender) {
            document.getElementById('genderError').textContent = "Gender is required";
            isValid = false;
        } else {
            document.getElementById('genderError').textContent = "";
        }

        // Validate location
        if (!location) {
            document.getElementById('locationError').textContent = "Location is required";
            isValid = false;
        } else {
            document.getElementById('locationError').textContent = "";
        }

        // Validate hospital name
        if (!hospitalName) {
            document.getElementById('hospitalError').textContent = "Hospital name is required";
            isValid = false;
        } else {
            document.getElementById('hospitalError').textContent = "";
        }

        // If the form is not valid, prevent submission
        if (!isValid) {
            event.preventDefault();
        }
    });

    // Function to validate email format
    function validateEmail(email) {
        const emailPattern = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
        return emailPattern.test(email);
    }

    // Function to validate phone number format
    function validatePhoneNumber(phone) {
        const phonePattern = /^\d{10}$/;
        return phonePattern.test(phone);
    }
    function clearError(elementId) {
        setTimeout(function() {
            document.getElementById(elementId).textContent = "";
        }, 5000);
    }

    document.getElementById('cancelButton').addEventListener('click', () => {
        window.location.href = "{{ url('/admin/viewDoctor') }}";
    });
  </script>
</body>
</html>
