<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Staff Movement System</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">

  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

  <link rel="stylesheet" href="{{ asset('assets/vendor/aos/aos.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap/css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap-icons/bootstrap-icons.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/boxicons/css/boxicons.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/glightbox/css/glightbox.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/swiper/swiper-bundle.min.css') }}">

  <link rel="stylesheet" href="{{ asset('assets/css/Adduser.css') }}">
</head>
<body>
  <i class="bi bi-list mobile-nav-toggle d-xl-none"></i>
  <header id="header">
    <div class="d-flex flex-column">
      <div class="profile">
        <img src="{{ asset('assets/img/logo.png') }}" alt="No Logo" class="img-fluid">
        <h1 class="text-light"><a href="index.html"></a></h1>
      </div>
      <nav id="navbar" class="nav-menu navbar">
        <ul>
            <li><a href="{{ url('/adm/dashboard') }}" class="nav-link scrollto"><i class="fas fa-home"></i> <span>DashBoard</span></a></li>
            <li><a href="{{ url('/adm/admViewDoctor') }}" class="nav-link scrollto active"><i class="fas fa-user-md"></i> <span>Doctor</span></a></li>
            <li><a href="{{ url('/adm/leave') }}" class="nav-link scrollto"><i class="fas fa-calendar-alt"></i> <span>Leave</span></a></li>
            <li><a href="{{ url('/adm/admProfile') }}" class="nav-link scrollto"><i class="fas fa-user-circle"></i> <span>Profile</span></a></li>
            <li>
                <form method="POST" action="{{ route('logout') }}">
                    @csrf
                    <a href="#"  class="nav-link scrollto" onclick="event.preventDefault();this.closest('form').submit();"><i class="fas fa-sign-out-alt"></i><span>Log Out</span></a>
                </form>
            </li>
      </ul>
      </nav>
    </div>
  </header>

<main id="main">
    <section id="hero" class="about">
    <div class="container text-left">
        <h4 style="font-weight:800; margin: 50px 0px;">Add Doctor</h4>

        @if(session('success'))
            <div class="alert alert-success" id="successAlert">
                {{ session('success') }}
            </div>
            <script>
                setTimeout(function() {
                    $('#successAlert').fadeOut('fast');
                }, 5000);
            </script>
        @endif

        @if(session('error'))
            <div id="error-alert" class="alert alert-danger">
                {{ session('error') }}
            </div>
            <script>
                setTimeout(function() {
                    var errorAlert = document.getElementById('error-alert');
                    if(errorAlert) {
                        errorAlert.style.display = 'none';
                    }
                }, 5000);
            </script>
        @endif

        <form id="form" method="POST" action="{{ route('admAddDoctor') }}"  enctype="multipart/form-data">
            {!! csrf_field() !!}
            <div class="form-row">
                <div class="col-lg-4 mb-lg-4 mr-lg-4 mb-3">
                    <label>Name</label>
                    <input type="text" name="name" id="name" class="form-control rounded-1">
                    <div id="nameError" class="error-message" style="color: red; font-size: 14px; font-weight: 600; margin-top: 5px"></div>
                </div>

                <div class="col-lg-4 mb-lg-4 ml-lg-4 mb-3">
                    <label>Email address</label>
                    <input type="email" name="email" id="email" class="form-control rounded-1" >
                    <div id="emailError" class="error-message" style="color: red; font-size: 14px; font-weight: 600; margin-top: 5px"></div>
                </div>
            </div>

            <div class="form-row">
                <div class="col-lg-4 mb-lg-4 mr-lg-4 mb-3">
                    <label>Phone Number</label>
                    <input type="text" name="phone" id="phone" class="form-control rounded-1" >
                    <div id="phoneError" class="error-message" style="color: red; font-size: 14px; font-weight: 600; margin-top: 5px"></div>
                </div>

                <div class="col-lg-4 mb-lg-4 ml-lg-4 mb-3">
                    <label>CID Number</label>
                    <input type="text" name="cid" id="cid" class="form-control rounded-1" >
                    <div id="cidError" class="error-message" style="color: red; font-size: 14px; font-weight: 600; margin-top: 5px"></div>
                </div>
            </div>

            <div class="form-row">
                <div class="col-lg-4 mr-lg-4 mr-lg-4 mb-lg-4 mb-3">
                    <label>Specialization</label>
                    <input type="text" name="specialization" id="specialization" class="form-control rounded-1" >
                    <div id="specializationError" class="error-message" style="color: red; font-size: 14px; font-weight: 600; margin-top: 5px"></div>
                </div>
                <div class="col-lg-4 mb-lg-4 ml-lg-4 mb-3">
                    <label>Gender</label>
                    <select name="gender" id="gender" class="form-control rounded-1">
                        <option value="male">Male</option>
                        <option value="female">Female</option>
                        <option value="other">Other</option>
                    </select>
                </div>
            </div>

            <div class="form-row">
                <div class="col-12 text-area">
                    <label>Background Details</label>
                    <textarea class="form-control rounded-1" name="description" id="description" rows="5"></textarea>
                    <div id="descriptionError" class="error-message" style="color: red; font-size: 14px; font-weight: 600; margin-top: 5px"></div>
                </div>
            </div>

            <div class="form-row">
                <div class="col-12">
                    <label for="image">Image</label>
                    <input type="file" name="image" id="image" class="form-control mt-4" style="font-size: 14px; width: 250px;"/>
                    <div id="imageError" class="error-message" style="color: red; font-size: 14px; font-weight: 600; margin-top: 5px"></div>
                </div>
            </div>

            <div class="d-md-flex justify-content-start align-items-center mt-4" style="margin-bottom: 40px;">
                <button class="btn btn-primary" type="submit">Save</button>
                <a href="{{ route('admAddDoctor') }}" class="btn btn-secondary">Cancel</a>
            </div>
        </form>
        </div>
    </section>
</main>

<script src="{{ asset('assets/js/main.js') }}"></script>
<script>
    const form = document.getElementById('form');
    const name = document.getElementById('name');
    const email = document.getElementById('email');
    const phone = document.getElementById('phone');
    const cid = document.getElementById('cid');
    const specialization = document.getElementById('specialization');
    const description = document.getElementById('description');
    const errorElement = document.getElementById('error');
    const nameError = document.getElementById('nameError');
    const emailError = document.getElementById('emailError');
    const phoneError = document.getElementById('phoneError');
    const cidError = document.getElementById('cidError');
    const specializationError = document.getElementById('specializationError');
    const descriptionError = document.getElementById('descriptionError');

    const image = document.getElementById('image');
    const imageError = document.getElementById('imageError');

    form.addEventListener('submit', (e) => {
        let messages = [];

        if (name.value.trim() === '') {
            messages.push('* Name is required');
            nameError.textContent = '* Name is required';
        } else {
            nameError.textContent = '';
        }

        if (!email.value.match(/^\S+@\S+\.\S+$/)) {
            messages.push('* Email is not valid');
            emailError.textContent = '* Email is not valid';
        } else {
            emailError.textContent = '';
        }

        if (!phone.value.match(/^(17|77)\d{6}$/) || phone.value.length !== 8) {
            messages.push('* Mobile number should start with 17 or 77 and be 8 digits long');
            phoneError.textContent = '* Mobile number should start with 17 or 77 and be 8 digits long';
        } else {
            phoneError.textContent = '';
        }

        if (cid.value.length !== 11) {
            messages.push('* CID number should be 11 digits long');
            cidError.textContent = '* CID number should be 11 digits long';
        } else {
            cidError.textContent = '';
        }

        if (specialization.value.trim() === '') {
            messages.push('* Specialization is required');
            specializationError.textContent = '* Specialization is required';
        } else {
            specializationError.textContent = '';
        }

        if (description.value.trim() === '') {
            messages.push('* Background details are required');
            descriptionError.textContent = '* Background details are required';
        } else {
            descriptionError.textContent = '';
        }

        if (!isValidImageType(image.files[0])) {
            messages.push('* Please select a valid image file (JPEG, PNG, or GIF)');
            imageError.textContent = '* Please select a valid image file (JPEG, PNG, or GIF)';
        } else {
            imageError.textContent = '';
        }

        if (messages.length > 0) {
            e.preventDefault();
        }
    });

    function isValidImageType(file) {
        const acceptedImageTypes = ['image/jpeg', 'image/png', 'image/gif'];
        return file && acceptedImageTypes.includes(file.type);
    }

    function displayErrors(messages) {
        errorElement.innerHTML = messages.join('<br>');
    }
</script>
</body>
</html>
