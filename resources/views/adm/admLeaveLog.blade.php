<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Staff Movement System</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">

  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <link rel="stylesheet" href="{{ asset('assets/vendor/aos/aos.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap/css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap-icons/bootstrap-icons.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/boxicons/css/boxicons.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/glightbox/css/glightbox.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/swiper/swiper-bundle.min.css') }}">

  <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/css/Doctor.css') }}">
</head>
<body>

  <i class="bi bi-list mobile-nav-toggle d-xl-none"></i>
  <header id="header">
    <div class="d-flex flex-column">
      <div class="profile">
        <img src="{{ asset('assets/img/logo.png') }}" alt="No Logo" class="img-fluid">
        <h1 class="text-light"><a href="index.html"></a></h1>
      </div>

      <nav id="navbar" class="nav-menu navbar">
        <ul>
          <li><a href="{{ url('/adm/dashboard') }}" class="nav-link scrollto"><i class="fas fa-home"></i> <span>DashBoard</span></a></li>
          <li><a href="{{ url('/adm/admViewDoctor') }}" class="nav-link scrollto active"><i class="fas fa-user-md"></i> <span>Doctor</span></a></li>
          <li><a href="{{ url('/adm/leave') }}" class="nav-link scrollto  "><i class="fas fa-calendar-alt"></i> <span>Leave</span></a></li>
          <li><a href="{{ url('/adm/admProfile') }}" class="nav-link scrollto"><i class="fas fa-user-circle"></i> <span>Profile</span></a></li>
          <li>
            <form method="POST" action="{{ route('logout') }}">
                @csrf
                <a href="#"  class="nav-link scrollto" onclick="event.preventDefault();this.closest('form').submit();"><i class="fas fa-sign-out-alt"></i><span>Log Out</span></a>
            </form>
        </li>
      </ul>
      </nav>
    </div>
  </header>

  <main id="main">
 <section id="hero" class="col-lg-10 m-auto">
      <div class="d-flex justify-content-between align-items-center " style="margin-bottom: 40px;">
        <h4 style="font-weight:800; margin: 0;">Leave Log</h4>
          <div class="d-none d-md-flex justify-content-start align-items-center">
              <button class="btn btn-primary me-2" type="button"><a href="{{ route('admdownloadUserLeave', ['cid' => $cid]) }}" style="text-decoration: none; color: rgb(255, 255, 255);">Download</a></button>
              <div class="btn-group">
                <button class="btn btn-secondary btn-sm dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                    Sort
                </button>
                <ul class="dropdown-menu">
                    <li><a class="dropdown-item" href="{{ route('admLeaveLog', ['cid' => $cid]) }}">All</a></li>
                    <li><a class="dropdown-item" href="{{ route('admLeaveLog', ['cid' => $cid, 'type' => 'leave']) }}">Leave</a></li>
                    <li><a class="dropdown-item" href="{{ route('admLeaveLog', ['cid' => $cid, 'type' => 'tour']) }}">Tour</a></li>
                    <li><a class="dropdown-item" href="{{ route('admLeaveLog', ['cid' => $cid, 'type' => 'event']) }}">Event</a></li>
                </ul>
            </div>
          </div>
      </div>
      <div class="d-md-none mt-3 pr-md-3" style="margin-bottom: 40px;" >
          <button class="btn btn-primary " type="button"><a href="{{ route('admdownloadUserLeave', ['cid' => $cid]) }}" style="text-decoration: none; color: rgb(255, 255, 255);">Download</a></button>
      </div>
  <table class="table border-dark">
    <thead class="table-primary">
      <tr>
        <th scope="col">#</th>
        <th scope="col">Start Date</th>
        <th scope="col">End Date</th>
        <th scope="col">Leave type</th>
        <th scope="col">Remarks</th>
      </tr>
    </thead>
    <tbody>
    @foreach($leave as $l)
        <tr>
          <th scope="row">{{ $loop->iteration }}</th>
          <td>{{ $l->start }}</td>
          <td>{{ $l->end }}</td>
          <td>{{ $l->type }}</td>
          <td>{{ $l->remarks }}</td>
        </tr>
    @endforeach
    </tbody>
  </table>
</section>
</main>

<script src="{{ asset('assets/js/main.js') }}"></script>
<script>
    function navigateToDetail(url) {
        window.location.href = url;
    }
</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js"></script>
</body>
</html>


