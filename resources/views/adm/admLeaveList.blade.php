<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta content="width=device-width, initial-scale=1.0" name="viewport" />

        <title>Staff Movement System</title>
        <meta content="" name="description" />
        <meta content="" name="keywords" />

        <link href="assets/img/favicon.png" rel="icon" />
        <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" />
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">

        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet" />

        <link rel="stylesheet" href="{{ asset('assets/vendor/aos/aos.css') }}" />
        <link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap/css/bootstrap.min.css') }}" />
        <link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap-icons/bootstrap-icons.css') }}" />
        <link rel="stylesheet" href="{{ asset('assets/vendor/boxicons/css/boxicons.min.css') }}" />
        <link rel="stylesheet" href="{{ asset('assets/vendor/glightbox/css/glightbox.min.css') }}" />
        <link rel="stylesheet" href="{{ asset('assets/vendor/swiper/swiper-bundle.min.css') }}" />

        <link rel="stylesheet" href="{{ asset('assets/css/Doctor.css') }}" />
    </head>

    <body>
        <i class="bi bi-list mobile-nav-toggle d-xl-none"></i>
        <header id="header">
            <div class="d-flex flex-column">
                <div class="profile">
                    <img src="{{ asset('assets/img/logo.png') }}" alt="No Logo" class="img-fluid" />
                    <h1 class="text-light"><a href="index.html"></a></h1>
                </div>
                <nav id="navbar" class="nav-menu navbar">
                    <ul>
                        <li>
                            <a href="{{ url('/adm/dashboard') }}" class="nav-link scrollto"><i class="fas fa-home"></i> <span>DashBoard</span></a>
                        </li>
                        <li>
                            <a href="{{ url('/adm/admViewDoctor') }}" class="nav-link scrollto"><i class="fas fa-user-md"></i> <span>Doctor</span></a>
                        </li>
                        <li>
                            <li><a href="{{ url('/adm/leave') }}" class="nav-link scrollto  active"><i class="fas fa-calendar-alt"></i> <span>Leave</span></a></li>
                        </li>
                        <li>
                            <a href="{{ url('/adm/admProfile') }}" class="nav-link scrollto"><i class="fas fa-user-circle"></i> <span>Profile</span></a>
                        </li>
                        <li>
                            <form method="POST" action="{{ route('logout') }}">
                                @csrf
                                <a href="#" class="nav-link scrollto" onclick="event.preventDefault();this.closest('form').submit();"><i class="fas fa-sign-out-alt"></i><span>Log Out</span></a>
                            </form>
                        </li>
                    </ul>
                </nav>
            </div>
        </header>

        <main id="main">
            <section id="hero" class="about">
                <div class="container text-left">
                    <div class="d-flex justify-content-between align-items-center" style="margin-bottom: 40px;">
                        <h4 style="font-weight: 800; margin: 0;">Edit Leave List</h4>
                    </div>

                    @if(session('success'))
                        <div class="alert alert-success" id="successAlert">
                            {{ session('success') }}
                        </div>
                        <script>
                            setTimeout(function() {
                                $('#successAlert').fadeOut('fast');
                            }, 5000);
                        </script>
                    @endif

                    @if(session('error'))
                        <div id="error-alert" class="alert alert-danger">
                            {{ session('error') }}
                        </div>
                        <script>
                            setTimeout(function() {
                                var errorAlert = document.getElementById('error-alert');
                                if(errorAlert) {
                                    errorAlert.style.display = 'none';
                                }
                            }, 5000);
                        </script>
                    @endif

                    @if($leaves->isNotEmpty())
                        @foreach($leaves as $leave)
                            <?php
                                $currentTime = now();
                                $leaveTime = max($leave->created_at, $leave->updated_at);
                                $timeDifference = $currentTime->diffInHours($leaveTime);
                            ?>
                            <div class="card col-12" style="padding: 10px">
                                <div style="cursor: pointer; width:100%; display:flex; flex-direction:row;">
                                    <img src="{{ asset('/storage/images/' . $leave->image) }}" alt="{{ $leave->name }}" class="card-img-top">
                                    <div class="card-body" style="display: flex;flex-direction: row; align-items: center;">
                                        <p class="card-text">
                                            <b>Name: {{ $leave->name }}</b><br>
                                            CID: {{ $leave->cid }}<br>
                                            Start Date: {{ \Carbon\Carbon::parse($leave->start)->format('d/m/Y') }}<br>
                                            End Date: {{ \Carbon\Carbon::parse($leave->end)->format('d/m/Y') }}<br>
                                            Type: {{ $leave->type }}<br>
                                        </p>
                                    </div>
                                </div>

                                @if($timeDifference <= 24)
                                    <div class="dropdown" style="cursor: pointer; margin-left: 10px">
                                        <i class="fas fa-ellipsis-h " id="ellipsisToggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></i>
                                        <div class="dropdown-menu" aria-labelledby="ellipsisToggle">
                                            <a class="dropdown-item" href="{{ route('admEditLeave', ['id' => $leave->id]) }}" title="Edit Leave">Edit</a>
                                            <a href="#" class="dropdown-item delete-btn" title="Cancel leave by Adm" data-toggle="modal" data-target="#cancelLeaveModal" data-id="{{ $leave->id }}">
                                                Cancel
                                            </a>
                                        </div>
                                    </div>
                                @else
                                    <p style="margin-left: 10px; color: rgb(223, 13, 13); font-size: 12px">The ability to edit or delete this entry has been disabled as it has been more than 24 hours since its creation or last update.</p>
                                @endif
                            </div>
                        @endforeach
                    @else
                        <p>No Leaves found.</p>
                    @endif

                    {{-- Cancel Leave Modal --}}
                    <div class="modal fade" id="cancelLeaveModal" tabindex="-1" role="dialog" aria-labelledby="cancelLeaveModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="cancelLeaveModalLabel">Confirm Deletion</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    Are you sure you want to cancel this leave?
                                </div>
                                <div class="modal-footer">
                                    <form method="POST" id="deleteForm" action="" accept-charset="UTF-8" style="display:inline">
                                        @method('DELETE')
                                        @csrf
                                        <button type="submit" class="btn btn-secondary">Cancel Leave</button>
                                    </form>
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </main>
    </body>
<script src="{{ asset('assets/js/main.js') }}"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>

<script>
    document.querySelectorAll('.delete-btn').forEach(button => {
        button.addEventListener('click', function() {
            var id = this.getAttribute('data-id');
            var route = "{{ route('admDeleteLeave', ['id' => ':id']) }}";
            route = route.replace(':id', id);
            document.getElementById('deleteForm').setAttribute('action', route);
        });
    });
</script>
</html>
