<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Staff Movement System</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">

  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <link rel="stylesheet" href="{{ asset('assets/vendor/aos/aos.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap/css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap-icons/bootstrap-icons.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/boxicons/css/boxicons.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/glightbox/css/glightbox.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/swiper/swiper-bundle.min.css') }}">

  <link rel="stylesheet" href="{{ asset('assets/css/Profile.css') }}">
</head>

<body>
  <i class="bi bi-list mobile-nav-toggle d-xl-none"></i>
  <header id="header">
    <div class="d-flex flex-column">
      <div class="profile">
        <img src="{{ asset('assets/img/logo.png') }}" alt="No Logo" class="img-fluid">
        <h1 class="text-light"><a href="index.html"></a></h1>
      </div>

      <nav id="navbar" class="nav-menu navbar">
        <ul>
            <li><a href="{{ url('/director/dashboard') }}" class="nav-link scrollto"><i class="fas fa-home"></i> <span>DashBoard</span></a></li>
            <li><a href="{{ url('/director/directorViewDoctor') }}" class="nav-link scrollto"><i class="fas fa-user-md"></i> <span>Doctor</span></a></li>
            <li><a href="{{ url('/director/directorProfile') }}" class="nav-link scrollto active"><i class="fas fa-user-circle"></i> <span>Profile</span></a></li>
            <li>
                <form method="POST" action="{{ route('logout') }}">
                    @csrf
                    <a href="#"  class="nav-link scrollto" onclick="event.preventDefault();this.closest('form').submit();"><i class="fas fa-sign-out-alt"></i><span>Log Out</span></a>
                </form>
            </li>
      </ul>
      </nav>
    </div>
  </header>


  <main id="main">
    <section id="hero" class="about">
      <div class="container">
        <h4 style="font-weight:800;">Profile DashBoard</h4>
        <div class="row justify-content-center">
          <div class="col-md-6" style="display: flex; flex-direction: column;margin-top: 50px;">
            <form method="POST" action="{{ route('directorUpdateProfile', ['cid' => $user->cid]) }}" enctype="multipart/form-data" class="col-lg-12" style="margin-top: 40px; margin-bottom: 40px">
                @method('PUT')
                @csrf

                <div class="col-lg-12 mb-3" style="display:flex; flex-direction: row;">
                    <label class="col-lg-4 mb-2" style="margin-right:20px">Name</label>
                    <input type="text" name="name" id="name" class="form-control rounded-1" value="{{ $user->name }}">
                </div>

                <div class="col-lg-12 mb-3" style="display:flex; flex-direction: row">
                    <label class="col-lg-4 mb-2" style="margin-right:20px">Email</label>
                    <input type="text" name="email" id="email" class="form-control rounded-1" value="{{ $user->email }}">
                </div>
                <div class="col-lg-12 mv-3 d-lg-flex flex-row d-block">
                    <label class="col-lg-4 mb-2" style="margin-right:20px">Contact</label>
                    <input type="number" name="phone" id="phone" class="form-control rounded-1" value="{{ $user->phone }}">
                </div>

                <input type="file" name="image" id="image" class="form-control col-lg-3 mt-4"  style="font-size: 14px;"/>

                <div class="d-md-flex justify-content-center align-items-center mt-4" style="margin-top: 40px;">
                    <div class="d-md-flex justify-content-start align-items-center mt-4" style="margin-bottom: 40px;">
                        <button class="btn btn-primary " type="submit">Save</button>
                        <a href="{{ route('directorProfile') }}" class="btn btn-secondary">Cancel</a>
                    </div>
                </div>
            </form>
          </div>
      </div>
      </div>
    </section>
  </main>
  <script src="{{ asset('assets/js/main.js') }}"></script>
</body>
</html>
