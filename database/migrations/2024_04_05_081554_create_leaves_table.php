<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('leaves', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('cid');
            $table->string('name');
            $table->date('start');
            $table->date('end');
            $table->string('type');
            $table->string('location');
            $table->string('hospitalName');
            $table->string('remarks',2000);
            $table->string("image")->nullable();
            // $table->string("profile")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('leaves');
    }
};
