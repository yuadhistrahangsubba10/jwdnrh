document.addEventListener('DOMContentLoaded', function () {
    var ctx = document.getElementById('myChart').getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
            datasets: [{
                label: 'On Leave',
                data: [20, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65], // Example data for "On Leave"
                borderColor: 'rgba(255, 99, 132, 1)',
                backgroundColor: 'rgba(255, 99, 132, 1)',
                borderWidth: 1,
                fill: false
            }, {
                label: 'On Tour',
                data: [10, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70, 75], // Example data for "On Tour"
                borderColor: 'rgba(75, 192, 192, 1)',
                backgroundColor: 'rgba(75, 192, 192, 1)',
                borderWidth: 1,
                fill: false
            }, {
                label: 'On Event',
                data: [20, 35, 40, 45, 50, 55, 60, 65, 70, 75, 80, 85], // Example data for "On Event"
                borderColor: 'rgba(54, 162, 235, 1)',
                backgroundColor: 'rgba(54, 162, 235, 1)',
                borderWidth: 1,
                fill: false
            }]
        },
        options: {
            scales: {
                xAxes: [{
                    scaleLabel: {
                        display: true,
                        labelString: 'Months'
                    }
                }],
                yAxes: [{
                    scaleLabel: {
                        display: true,
                        labelString: 'Values'
                    },
                    ticks: {
                        beginAtZero: true,
                        stepSize: 10,
                        max: 100
                    }
                }]
            }
        }
    });
});
