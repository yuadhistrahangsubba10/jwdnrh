<?php

use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Backend\DirectorController;
use App\Http\Controllers\Backend\AdminController;
use App\Http\Controllers\Backend\AdmController;
use App\Http\Controllers\Backend\ForgotController;
use App\Http\Controllers\Backend\LoginController;
use App\Http\Controllers\Backend\HospitalController;
use App\Http\Controllers\Backend\DownloadController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::middleware('guest')->group(function () {
    Route::get('/', [LoginController::class, 'login'])->name('Login');
    Route::get('/forgot', [ForgotController::class, 'forgot'])->name('forgot');
});

Route::middleware(['auth','role:admin'])->group(function () {
    Route::get('/admin/dashboard', [AdminController::class, 'adminDashboard'])->name('admin.dashboard');
    Route::get('/admin/dashboard/getDoctorsOnLeave', [AdminController::class, 'getDoctorsOnLeave']);

      //Display doctors, hospitals, directors and  Adm
      Route::get('/admin/viewDoctor', [AdminController::class, 'doctor'])->name('viewDoctor');
      Route::get('/admin/viewDirector', [AdminController::class, 'director'])->name('viewDirector');
      Route::get('/admin/viewAdm', [AdminController::class, 'adm'])->name('viewAdm');
      Route::get('/admin/viewHospital', [AdminController::class, 'hospital'])->name('viewHospital');

      // ADM
      Route::get('admin/viewAdm', [AdminController::class, 'callAdm'])->name('callAdm');
      Route::get('/admin/addAdm', [AdminController::class, 'AdmPage'])->name('addAdm');
      Route::post('/admin/addAdm', [AdminController::class, 'addAdmPost'])->name('createAdm');
      Route::delete('/admin/viewAdm/{cid}', [AdminController::class, 'deleteAdm'])->name('deleteAdm');
      Route::get('/admin/viewAdm/{cid}/edit', [AdminController::class, 'editAdm'])->name('editAdm');
      Route::put('/admin/viewAdm/{cid}', [AdminController::class, 'updateAdm'])->name('updateAdm');

      // Director
      Route::get('admin/viewDirector', [AdminController::class, 'callDirectors'])->name('callDirectors');
      Route::get('/admin/addDirector', [AdminController::class, 'DirectorPage'])->name('addDirector');
      Route::post('/admin/addDirector', [AdminController::class, 'addDirectorPost'])->name('createDirector');
      Route::delete('/admin/viewDirector/{cid}', [AdminController::class, 'deleteDirector'])->name('deleteDirector');
      Route::get('/admin/viewDirector/{cid}/edit', [AdminController::class, 'editDirector'])->name('editDirector');
      Route::put('/admin/viewDirector/{cid}', [AdminController::class, 'updateDirector'])->name('updateDirector');

      // Hospital
      Route::get('admin/viewHospital', [AdminController::class, 'callHospitals'])->name('callHospitals');
      Route::get('/admin/addHospital', [AdminController::class, 'hospitalPage'])->name('addHospital');
      Route::post('/admin/addHospital', [HospitalController::class, 'addHospitalPost'])->name('createHospital');
      Route::delete('/admin/viewHospital/{id}', [HospitalController::class, 'deleteHospital'])->name('deleteHospital');
      Route::get('/admin/viewHospital/{id}/edit', [HospitalController::class, 'editHospital'])->name('editHospital');
      Route::put('/admin/viewHospital/{id}', [HospitalController::class, 'updateHospital'])->name('updateHospital');

      // Doctor
      Route::get('admin/viewDoctor', [AdminController::class, 'callDoctors'])->name('callDoctors');
      Route::get('/admin/addDoctor', [AdminController::class, 'doctorPage'])->name('addDoctor');
      Route::post('/admin/addDoctor', [AdminController::class, 'addDoctorPost'])->name('createDoctor');
      Route::get('/admin/viewDoctorDetail/{cid}', [AdminController::class, 'adminViewDoctor'])->name('adminViewDoctor');
      Route::delete('/admin/viewDoctor/{cid}', [AdminController::class, 'deleteDoctor'])->name('deleteDoctor');
      Route::get('/admin/viewDoctor/{cid}/edit', [AdminController::class, 'editDoctor'])->name('editDoctor');
      Route::put('/admin/viewDoctor/{cid}', [AdminController::class, 'updateDoctor'])->name('updateDoctor');

      // Profile
      Route::get('/admin/adminProfile',[AdminController::class,'adminProfile'])->name('adminProfile');
      Route::get('/admin/adminProfile/{cid}/edit', [AdminController::class, 'adminEditProfile'])->name('adminEditProfile');
      Route::put('/admin/adminProfile/{cid}', [AdminController::class, 'adminUpdateProfile'])->name('adminUpdateProfile');
      Route::get('/admin/adminResetPassword/{cid}/edit', [AdminController::class, 'adminResetPassword'])->name('adminResetPassword');

      //Download
      Route::get('/admin/downloadHospital', [DownloadController::class, 'downloadHospital'])->name('downloadHospital');
      Route::get('/admin/downloadDoctor', [DownloadController::class, 'downloadDoctor'])->name('downloadDoctor');
      Route::get('/admin/downloadDirector', [DownloadController::class, 'downloadDirector'])->name('downloadDirector');
      Route::get('/admin/downloadAdm', [DownloadController::class, 'downloadAdm'])->name('downloadAdm');

});

Route::middleware(['auth','role:adm'])->group(function () {
    Route::get('/adm/dashboard', [AdmController::class, 'admDashboard'])->name('adm.dashboard');
    Route::get('/adm/dashboard/getDoctorsOnLeave', [AdmController::class, 'getDoctorsOnLeave']);

    // Doctor
    Route::get('/adm/admViewDoctor', [AdmController::class, 'admDoctorList'])->name('adm.admViewDoctor');
    Route::get('/adm/admAddDoctor', [AdmController::class, 'admDoctorPage'])->name('admAddDoctor');
    Route::post('/adm/admAddDoctor', [AdmController::class, 'admAddDoctorPost'])->name('admCreateDoctor');
    Route::get('/adm/admViewDoctorDetail/{cid}', [AdmController::class, 'admViewDoctorDetail'])->name('admViewDoctorDetail');
    Route::get('/adm/admLeaveLog/{cid}/download', [DownloadController::class, 'downloadUserLeave'])->name('admdownloadUserLeave');
    Route::get('/adm/admLeaveLog/{cid}/{type?}', [AdmController::class, 'admLeaveLog'])->name('admLeaveLog');
    Route::delete('/adm/admViewDoctor/{cid}', [AdmController::class, 'admDeleteDoctor'])->name('admDeleteDoctor');
    Route::get('/adm/admViewDoctor/{cid}/edit', [AdmController::class, 'admEditDoctor'])->name('admEditDoctor');
    Route::put('/adm/admViewDoctor/{cid}', [AdmController::class, 'admUpdateDoctor'])->name('admUpdateDoctor');

    // Profile
    Route::get('/adm/admProfile',[AdmController::class,'admProfile'])->name('admProfile');
    Route::get('/adm/admProfile/{cid}/edit', [AdmController::class, 'admEditProfile'])->name('admEditProfile');
    Route::put('/adm/editProfile/{cid}', [AdmController::class, 'admUpdateProfile'])->name('admUpdateProfile');
    Route::get('/adm/resetPassword/{cid}/edit', [AdmController::class, 'admResetPassword'])->name('admResetPassword');

    // Leave
    Route::get('/adm/leave',[AdmController::class,'leave'])->name('leave');
    Route::get('/adm/leave',[AdmController::class,'admViewLeave'])->name('admViewLeave');
    Route::get('/adm/admAddLeave', [AdmController::class, 'admLeavePage'])->name('admAddLeave');
    Route::post('/adm/admAddLeave', [AdmController::class, 'admLeavePost'])->name('admCreateLeave');

    Route::get('/adm/admLeaveList',[AdmController::class,'getLeaveList'])->name('getLeaveList');
    Route::delete('/adm/admLeaveList/{id}', [AdmController::class, 'admDeleteLeave'])->name('admDeleteLeave');
    Route::get('/adm/admEditLeave/{id}/edit', [AdmController::class, 'admEditLeave'])->name('admEditLeave');
    Route::put('/adm/leaveUpdate/{id}', [AdmController::class, 'admUpdateLeave'])->name('admUpdateLeave');

    //Download
    Route::get('/adm/downloadLeaves', [DownloadController::class, 'downloadLeave'])->name('downloadLeave');
    Route::get('/adm/admDownloadDoctor', [DownloadController::class, 'admDownloadDoctor'])->name('admDownloadDoctor');

});

Route::middleware(['auth','role:director'])->group(function () {
    Route::get('/director/dashboard', [DirectorController::class, 'directorDashboard'])->name('director.dashboard');
    Route::get('/director/dashboard/getDoctorsOnLeave', [DirectorController::class, 'getDoctorsOnLeave']);

    //Display doctors
     Route::get('/director/directorViewDoctor', [DirectorController::class, 'directorDoctor'])->name('directorDoctor');
     Route::get('/director/directorViewDoctorList/{name}', [DirectorController::class, 'directorViewDoctorList'])->name('directorViewDoctorList');
     Route::get('/director/directorDoctorDetail/{cid}', [DirectorController::class, 'directorDoctorDetail'])->name('directorDoctorDetail');
     Route::get('/director/leaveLog/{cid}/download', [DownloadController::class, 'downloadUserLeave'])->name('downloadUserLeave');
     Route::get('/director/leaveLog/{cid}/{type?}', [DirectorController::class, 'directorLeaveLog'])->name('directorLeaveLog');

    // Profile
    Route::get('/director/directorProfile',[DirectorController::class,'directorProfile'])->name('directorProfile');
    Route::get('/director/directorProfile/{cid}/edit', [DirectorController::class, 'directorEditProfile'])->name('directorEditProfile');
    Route::put('/director/directorProfile/{cid}', [DirectorController::class, 'directorUpdateProfile'])->name('directorUpdateProfile');
    Route::get('/director/directorResetPassword/{cid}/edit', [DirectorController::class, 'directorResetPassword'])->name('directorResetPassword');

    //Download
    // Route::get('/director/directorDownloadDoctor', [DownloadController::class, 'directorDownloadDoctor'])->name('directorDownloadDoctor');

});

require __DIR__.'/auth.php';
