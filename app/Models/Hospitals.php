<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Notifications\Notifiable;

class Hospitals extends Model
{
    use HasApiTokens, HasFactory, Notifiable;

    protected $table = 'hospitals';
    protected $fillable = [
        'location',
        'name',
        'hospitalImage',
    ];
}
