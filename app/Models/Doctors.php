<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Doctors extends Model
{
    use HasFactory;

    protected $primaryKey = 'cid';
    protected $table = 'doctors';
    protected $fillable = [
        'cid',
        'email',
        'name',
        'phone',
        'specialization',
        'gender',
        'location',
        'hospitalName',
        'description',
        'image',
    ];
}
