<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\Hospitals;
use App\Models\Doctors;
use App\Models\User;
use App\Models\Leaves;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\Mail\NewPasswordMail;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    public function adminDashboard(){
        $recent = Leaves::all();
        $data = DB::table('leaves')
            ->selectRaw('MONTH(start) as month, COUNT(*) as count, type')
            ->groupBy('month', 'type')
            ->get();
        return view('admin.dashboard', [
            'recent' => $recent,
            'data' => $data,
    ]);
    }
    //Display User
    public function doctor(){
        return view('admin.viewDoctor');
    }
    public function director(){
        return view('admin.viewDirector');
    }
    public function hospital(){

        return view('admin.viewHospital');
    }
    public function adm(){
        return view('admin.viewAdm');
    }

    //Profile
    public function adminProfile(){
        $user = Auth::user();

        return view('admin.adminProfile', compact('user'));
    }

    public function adminEditProfile($cid){
        $users = User::where('cid', $cid)->first();

        return view('admin.adminEditProfile', ['user' => $users]);
    }

    public function adminUpdateProfile(Request $request, $cid){
        $user = User::find($cid);

        if (!$user) {
            return redirect()->back()->with('error', 'User not found.');
        }

        $request->validate([
            'name' => 'required|string',
            'email' => 'required|string',
            'phone' => 'required|string',
            'image' => 'nullable|image|mimes:jpeg,png,jpg,gif|max:2048',
        ]);

        if ($request->hasFile('image')) {
            if ($user->image) {
                Storage::disk('public')->delete('images/' . $user->image);
            }

            $image = $request->file('image');
            $imageName = time() . '.' . $image->getClientOriginalExtension();
            $image->storeAs('images', $imageName, 'public');
            $user->image = $imageName;
        }

        $user->name = $request->name;
        $user->email = $request->email;
        $user->phone = $request->phone;

        $user->save();

        return redirect()->route('adminProfile')->with('success', 'User updated successfully.');
    }

    public function adminResetPassword($cid){
        $users = User::where('cid', $cid)->first();

        return view('admin.resetPassword', ['user' => $users]);
    }

    // ADM
    public function admPage(){
        return view('admin.addAdm');
    }

     public function callAdm(){
        $users = User::where('role', 'adm')->get();
        return view('admin.viewAdm', compact('users'));
    }

    public function addAdmPost(Request $request) {

        $request->validate([
            'name' => 'required|string',
            'email' => 'required|string',
            'phone' => 'required|string',
            'cid' => 'required|string',
            'location' => 'required|string',
            'hospitalName' => 'required|string',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048',
        ]);

        $duplicateUser =  User::where('cid', $request->cid)->first();
        $duplicateEmail =  User::where('email', $request->email)->first();

        if ($duplicateUser) {
            return redirect()->route('addAdm')->with('error', 'A user with this CID already exists.');
        }

        if ($duplicateEmail) {
            return redirect()->route('addAdm')->with('error', 'A user with this Email already exists.');
        }

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $imageName = time() . '.' . $image->getClientOriginalExtension();
            $image->storeAs('images', $imageName, 'public');
        } else {
            $imageName = null;
        }

        $password = Str::random(10);
        $hashedPassword = Hash::make($password);

        $users = new User();

        $users->name = $request->name;
        $users->email = $request->email;
        $users->phone = $request->phone;
        $users->cid = $request->cid;
        $users->location = $request->location;
        $users->hospitalName = $request->hospitalName;
        $users->role = 'adm';
        $users->image = $imageName;
        $users->password = $hashedPassword;
        $users->save();

        Mail::to($users->email)->send(new NewPasswordMail($password));

        return redirect()->route('callAdm')->with('success', 'Administrative Assistant successfully added.');
    }

    public function deleteAdm($cid){
        $users = User::where('cid', $cid)->first();

        if ($users) {
            $users->delete();
            return redirect()->back()->with('success', 'Administrative Assistant deleted successfully.');
        } else {
            return redirect()->back()->with('error', 'Administrative Assistant not found.');
        }
    }

    public function editAdm($cid){
        $users = User::where('cid', $cid)->first();

        return view('admin.editAdm', ['user' => $users]);
    }

    public function updateAdm(Request $request, $cid){
        $user = User::find($cid);

        if (!$user) {
            return redirect()->back()->with('error', 'Administrative Assistant not found.');
        }

        $request->validate([
            'name' => 'required|string',
            'email' => 'required|string',
            'phone' => 'required|string',
            'cid' => 'required|string',
            'location' => 'required|string',
            'hospitalName' => 'required|string',
            'image' => 'nullable|image|mimes:jpeg,png,jpg,gif|max:2048',
        ]);
        if ($request->hasFile('image')) {
            if ($user->image) {
                Storage::disk('public')->delete('images/' . $user->image);
            }

            $image = $request->file('image');
            $imageName = time() . '.' . $image->getClientOriginalExtension();
            $image->storeAs('images', $imageName, 'public');

            $user->image = $imageName;
        }

        $user->name = $request->name;
        $user->email = $request->email;
        $user->phone = $request->phone;
        $user->cid= $request->cid;
        $user->location = $request->location;
        $user->hospitalName = $request->hospitalName;

        $user->save();

        return redirect()->route('callAdm')->with('success', 'Administrative Assistant updated successfully.');
    }

    // Director
    public function directorPage(){
        return view('admin.addDirector');
    }

    public function callDirectors(){
        $users = User::where('role', 'director')->get();
        return view('admin.viewDirector', compact('users'));
    }

    public function addDirectorPost(Request $request) {
        $duplicateUser =  User::where('cid', $request->cid)->first();
        $duplicateEmail =  User::where('email', $request->email)->first();

        if ($duplicateUser) {
            return redirect()->route('addDirector')->with('error', 'A user with this CID already exists.');
        }

        if ($duplicateEmail) {
            return redirect()->route('addDirector')->with('error', 'A user with this Email already exists.');
        }

        $request->validate([
            'name' => 'required|string',
            'email' => 'required|string',
            'phone' => 'required|string',
            'cid' => 'required|string',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048',
        ]);

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $imageName = time() . '.' . $image->getClientOriginalExtension();
            $image->storeAs('images', $imageName, 'public');
        } else {
            $imageName = null;
        }

        $password = Str::random(10);
        $hashedPassword = Hash::make($password);

        $users = new User();

        $users->name = $request->name;
        $users->email = $request->email;
        $users->phone = $request->phone;
        $users->cid = $request->cid;
        $users->location = 'Thimphu';
        $users->hospitalName = 'JDWNRH';
        $users->role = 'director';
        $users->image = $imageName;
        $users->password = $hashedPassword;
        $users->save();

        Mail::to($users->email)->send(new NewPasswordMail($password));

        return redirect()->route('callDirectors')->with('success', 'Director successfully added.');
    }

    public function deleteDirector($cid){
        $users = User::where('cid', $cid)->first();

        if ($users) {
            $users->delete();
            return redirect()->back()->with('success', 'Director deleted successfully.');
        } else {
            return redirect()->back()->with('error', 'Director not found.');
        }
    }

    public function editDirector($cid){
        $users = User::where('cid', $cid)->first();

        return view('admin.editDirector', ['user' => $users]);
    }

    public function updateDirector(Request $request, $cid){
        $user = User::find($cid);

        if (!$user) {
            return redirect()->back()->with('error', 'Director not found.');
        }
        $request->validate([
            'name' => 'required|string',
            'email' => 'required|string',
            'phone' => 'required|string',
            'cid' => 'required|string',
            'image' => 'nullable|image|mimes:jpeg,png,jpg,gif|max:2048',
        ]);

        if ($request->hasFile('image')) {
            if ($user->image) {
                Storage::disk('public')->delete('images/' . $user->image);
            }

            $image = $request->file('image');
            $imageName = time() . '.' . $image->getClientOriginalExtension();
            $image->storeAs('images', $imageName, 'public');

            $user->image = $imageName;
        }

        $user->name = $request->name;
        $user->email = $request->email;
        $user->phone = $request->phone;
        $user->cid= $request->cid;

        $user->save();

        return redirect()->route('callDirectors')->with('success', 'Director updated successfully.');
    }


    // Doctor
    public function doctorPage(){
        return view('admin.addDoctor');
    }
     // Call all Doctor
     public function callDoctors(){
        $doctors = Doctors::all();
        return view('admin.viewDoctor', compact('doctors'));
    }

    // View Doctor
    public function adminViewDoctor($cid){
        $doctor = Doctors::where('cid', $cid)->first();

        return view('admin.viewDoctorDetail', ['doctor' => $doctor]);
    }

    public function addDoctorPost(Request $request) {

        $duplicateUser =  Doctors::where('cid', $request->cid)->first();
        $duplicateEmail =  Doctors::where('email', $request->email)->first();

        if ($duplicateUser) {
            return redirect()->route('addDoctor')->with('error', 'A user with this CID already exists.');
        }

        if ($duplicateEmail) {
            return redirect()->route('addDoctor')->with('error', 'A user with this Email already exists.');
        }

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $imageName = time() . '.' . $image->getClientOriginalExtension();
            $image->storeAs('images', $imageName, 'public');
        } else {
            $imageName = null;
        }

        $doctors = new Doctors();

        $doctors->cid = $request->cid;
        $doctors->email = $request->email;
        $doctors->name = $request->name;
        $doctors->phone = $request->phone;
        $doctors->specialization = $request->specialization;
        $doctors->gender = $request->gender;
        $doctors->location = $request->location;
        $doctors->hospitalName = $request->hospitalName;
        $doctors->description = $request->description;
        $doctors->image = $imageName;

        $doctors->save();

        return redirect()->route('callDoctors')->with('success', 'Doctor successfully added.');
    }

    public function deleteDoctor($cid){
        $doctors = Doctors::where('cid', $cid)->first();

        if ($doctors) {
            $doctors->delete();
            return redirect()->back()->with('success', 'Doctor deleted successfully.');
        } else {
            return redirect()->back()->with('error', 'Doctor not found.');
        }
    }

    public function editDoctor($cid){
        $doctors = Doctors::where('cid', $cid)->first();

        return view('admin.editDoctor', ['doctor' => $doctors]);
    }

    public function updateDoctor(Request $request, $cid){
        $doctor = Doctors::find($cid);

        if (!$doctor) {
            return redirect()->back()->with('error', 'Doctor not found.');
        }

        $request->validate([
            'name' => 'required|string',
            'email' => 'required|string',
            'phone' => 'required|string',
            'cid' => 'required|string',
            'specialization' => 'required|string',
            'gender' => 'required|string',
            'location' => 'required|string',
            'hospitalName' => 'required|string',
            'description' => 'required|string',
            'image' => 'nullable|image|mimes:jpeg,png,jpg,gif|max:2048',
        ]);

        if ($request->hasFile('image')) {
            if ($doctor->image) {
                Storage::disk('public')->delete('images/' . $doctor->image);
            }

            $image = $request->file('image');
            $imageName = time() . '.' . $image->getClientOriginalExtension();
            $image->storeAs('images', $imageName, 'public');

            $doctor->image = $imageName;
        }

        $doctor->name = $request->name;
        $doctor->email = $request->email;
        $doctor->phone = $request->phone;
        $doctor->cid= $request->cid;
        $doctor->specialization = $request->specialization;
        $doctor->gender = $request->gender;
        $doctor->location = $request->location;
        $doctor->hospitalName = $request->hospitalName;
        $doctor->description = $request->description;

        $doctor->save();

        return redirect()->route('callDoctors')->with('success', 'Doctor updated successfully.');
    }

    // Hospital
    public function hospitalPage(){
        return view('admin.addHospital');
    }
    // Call all hospitals
    public function callHospitals(){
        // $hospitals = Hospitals::paginate(5);
        $hospitals = Hospitals::All();
        return view('admin.viewHospital', compact('hospitals'));
    }

    // Chat
    public function getDoctorsOnLeave(Request $request)
        {
            $month = $request->input('month');
            $leaveType = $request->input('leaveType');

            $doctorsOnLeave = DB::table('leaves')
                ->whereMonth('start', $month)
                ->where('type', $leaveType)
                ->select('name', 'hospitalName', 'cid')
                ->get();

            return response()->json($doctorsOnLeave);
        }
}
