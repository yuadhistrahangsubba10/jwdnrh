<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Doctors;
use App\Models\Leaves;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;
use App\Models\User;
use Illuminate\Validation\Rules\Password;
use Illuminate\Support\Facades\Hash;

class AdmController extends Controller
{
    public function admDashboard(){
        $user = Auth::user();
        $recent = Leaves::where('location', $user->location)
            ->where('hospitalName', $user->hospitalName)
            ->get();
            $data = DB::table('leaves')
            ->where('location', $user->location)
            ->where('hospitalName', $user->hospitalName)
            ->selectRaw('MONTH(start) as month, COUNT(*) as count, type')
            ->groupBy('month', 'type')
            ->get();

        return view('adm.dashboard', [
            'recent' => $recent,
            'data' => $data,
        ]);
    }


    //Profile
    public function admProfile(){
        $user = Auth::user();

        return view('adm.admProfile', compact('user'));
    }

    public function admEditProfile($cid){
        $users = User::where('cid', $cid)->first();

        return view('adm.editProfile', ['user' => $users]);
    }
    public function admUpdateProfile(Request $request, $cid){
        $user = User::find($cid);

        if (!$user) {
            return redirect()->back()->with('error', 'User not found.');
        }

        $request->validate([
            'name' => 'required|string',
            'email' => 'required|string',
            'phone' => 'required|string',
            'image' => 'nullable|image|mimes:jpeg,png,jpg,gif|max:2048',
        ]);

        if ($request->hasFile('image')) {
            if ($user->image) {
                Storage::disk('public')->delete('images/' . $user->image);
            }

            $image = $request->file('image');
            $imageName = time() . '.' . $image->getClientOriginalExtension();
            $image->storeAs('images', $imageName, 'public');

            $user->image = $imageName;
        }

        $user->name = $request->name;
        $user->email = $request->email;
        $user->phone = $request->phone;

        $user->save();

        return redirect()->route('admProfile')->with('success', 'User updated successfully.');
    }

    public function admResetPassword($cid){
        $users = User::where('cid', $cid)->first();

        return view('adm.resetPassword', ['user' => $users]);
    }

    //Display User
    public function admDoctorList(){
         $user = Auth::user();

        $doctors = Doctors::where('location', $user->location)
        ->where('hospitalName', $user->hospitalName)
        ->get();

        return view('adm.admViewDoctor', compact('doctors'));
    }

    //Add Doctor
    public function admDoctorPage(){
        return view('adm.admAddDoctor');
    }

    public function admViewDoctorDetail($cid){
        $doctor = Doctors::where('cid', $cid)->first();

        return view('adm.admViewDoctorDetail', ['doctor' => $doctor]);
    }

    public function admAddDoctorPost(Request $request) {
        $user = Auth::user();

        $duplicateUser =  Doctors::where('cid', $request->cid)->first();
        $duplicateEmail =  Doctors::where('email', $request->email)->first();

        if ($duplicateUser) {
            return redirect()->route('admAddDoctor')->with('error', 'A user with this CID already exists.');
        }

        if ($duplicateEmail) {
            return redirect()->route('admAddDoctor')->with('error', 'A user with this Email already exists.');
        }

        $request->validate([
            'cid' => 'required|string',
            'email' => 'required|string',
            'name' => 'required|string',
            'phone' => 'required|string',
            'specialization' => 'required|string',
            'gender' => 'required|string',
            'description' => 'required|string',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048',
        ]);

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $imageName = time() . '.' . $image->getClientOriginalExtension();
            $image->storeAs('images', $imageName, 'public');
        } else {
            $imageName = null;
        }

        $doctors = new Doctors();

        $doctors->cid = $request->cid;
        $doctors->email = $request->email;
        $doctors->name = $request->name;
        $doctors->phone = $request->phone;
        $doctors->specialization = $request->specialization;
        $doctors->gender = $request->gender;
        $doctors->hospitalName = $user->hospitalName;
        $doctors->location = $user->location;
        $doctors->description = $request->description;
        $doctors->image = $imageName;

        $doctors->save();

        return redirect()->route('adm.admViewDoctor')->with('success', 'Doctor successfully added.');
    }

    public function admEditDoctor($cid){
        $doctors = Doctors::where('cid', $cid)->first();

        return view('adm.admEditDoctor', ['doctor' => $doctors]);
    }

    public function admUpdateDoctor(Request $request, $cid){
        $doctor = Doctors::find($cid);

        if (!$doctor) {
            return redirect()->back()->with('error', 'Doctor not found.');
        }

        $request->validate([
            'cid' => 'required|string',
            'name' => 'required|string',
            'email' => 'required|string',
            'phone' => 'required|string',
            'specialization' => 'required|string',
            'gender' => 'required|string',
            'location' => 'required|string',
            'hospitalName' => 'required|string',
            'description' => 'required|string',
            'image' => 'nullable|image|mimes:jpeg,png,jpg,gif|max:2048',
        ]);

        if ($request->hasFile('image')) {
            if ($doctor->image) {
                Storage::disk('public')->delete('images/' . $doctor->image);
            }

            $image = $request->file('image');
            $imageName = time() . '.' . $image->getClientOriginalExtension();
            $image->storeAs('images', $imageName, 'public');

            $doctor->image = $imageName;
        }

        $doctor->name = $request->name;
        $doctor->email = $request->email;
        $doctor->phone = $request->phone;
        $doctor->cid= $request->cid;
        $doctor->specialization = $request->specialization;
        $doctor->gender = $request->gender;
        $doctor->location = $request->location;
        $doctor->hospitalName = $request->hospitalName;
        $doctor->description = $request->description;

        $doctor->save();

        return redirect()->route('adm.admViewDoctor')->with('success', 'Doctor updated successfully.');
    }

    public function admDeleteDoctor($cid){
        $doctors = Doctors::where('cid', $cid)->first();

        if ($doctors) {
            $doctors->delete();
            return redirect()->back()->with('success', 'Doctor deleted successfully.');
        } else {
            return redirect()->back()->with('error', 'Doctor not found.');
        }
    }

    // Add Leave
    public function leave(){
        return view('adm.leave');
    }

    public function admLeaveLog($cid, $type = null){
        if ($type) {
            $leave = Leaves::where('cid', $cid)
                        ->where('type', $type)
                        ->get();
        } else {
            $leave = Leaves::where('cid', $cid)->get();
        }
        return view('adm.admLeaveLog', ['leave' => $leave, 'cid' => $cid]);
    }

    public function admViewLeave() {
        $user = Auth::user();
        $leaves = Leaves::all();

        $doctors = Doctors::where('location', $user->location)
                          ->where('hospitalName', $user->hospitalName)
                          ->get();

        $doctorStatus = [];

        foreach ($doctors as $doctor) {
            // Getting the latest leave for the doctor
            $latestLeave = Leaves::where('cid', $doctor->cid)
                                 ->orderBy('start', 'desc')
                                 ->first();

            if ($latestLeave) {
                // Using Carbon to get the current date in the desired timezone
                $currentDate = Carbon::now('Asia/Thimphu')->toDateString(); // 'Y-m-d' format

                if ($currentDate >= $latestLeave->start && $currentDate <= $latestLeave->end) {
                    // Current date is within the leave period
                    $status = 'Not available';
                } else {
                    // Current date is not within the leave period
                    $status = 'Available';
                }
            } else {
                // Doctor's CID is not present in the leaves table
                $status = 'Available';
            }

            // Storing the status of each doctor
            $doctorStatus[$doctor->cid] = $status;
        }

        return view('adm.leave', ['doctors' => $doctors, 'leaves' => $leaves, 'doctorStatus' => $doctorStatus]);
    }

    public function admLeavePage(){
        return view('adm.admAddLeave');
    }

    public function admLeavePost(Request $request) {
        // Custom messages
        $customMessages = [
            'cid.required' => 'The CID field is required.',
            'type.required' => 'The type field is required.',
            'type.string' => 'The type must be a string.',
            'start.required' => 'The start date is required.',
            'start.date' => 'The starting date is not a valid date.',
            'end.required' => 'The end date is required.',
            'end.date' => 'The ending date is not a valid date.',
            'remarks.required' => 'The remarks field is required.',
            'remarks.string' => 'The remarks must be a string.',
            'image.image' => 'The image must be an image file.',
            'image.mimes' => 'The image must be a file of type: jpeg, png, jpg, gif.',
            'image.max' => 'The image must not be greater than 2048 kilobytes.',
            'image.required' => 'The image is required.',
        ];

        // Validation rules
        $request->validate([
            'cid' => 'required',
            'type' => 'required|string',
            'start' => 'required|date',
            'end' => 'required|date',
            'remarks' => 'required|string',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048',
        ], $customMessages);

        $user = Auth::user();

        $doctor = Doctors::where('cid', $request->cid)
                         ->where('hospitalName', $user->hospitalName)
                         ->first();

        if (!$doctor) {
            return redirect()->back()->with('error', 'Doctor with provided CID not found.');
        }

        // Check for leave collision
        $leaveCollisions = Leaves::where('cid', $doctor->cid)
            ->where(function($query) use ($request) {
                $query->whereBetween('start', [$request->start, $request->end])
                      ->orWhereBetween('end', [$request->start, $request->end])
                      ->orWhere(function($query) use ($request) {
                          $query->where('start', '<', $request->start)
                                ->where('end', '>', $request->end);
                      });
            })
            ->exists();

        if ($leaveCollisions) {
            return redirect()->route('admViewLeave')->with('error', 'Leave dates collide with existing leaves for this doctor.');
        }
        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $imageName = time() . '.' . $image->getClientOriginalExtension();
            $image->storeAs('images', $imageName, 'public');
        } else {
            $imageName = null;
        }

        $leave = new Leaves();
        $leave->cid = $doctor->cid;
        $leave->name = $doctor->name;
        $leave->start = $request->start;
        $leave->end = $request->end;
        $leave->location = $doctor->location;
        $leave->hospitalName = $doctor->hospitalName;
        $leave->type = $request->type;
        $leave->remarks = $request->remarks;
        $leave->image = $imageName;

        $leave->save();

        return redirect()->route('admViewLeave')->with('success', 'Leave successfully added.');
    }

    public function getLeaveList() {
    $user = Auth::user();

    $currentMonth = now()->format('m');

    $leaves = Leaves::where('location', $user->location)
                    ->where('hospitalName', $user->hospitalName)
                    ->whereMonth('start', $currentMonth)
                    ->orderBy('created_at', 'desc')
                    ->get();

    return view('adm.admLeaveList', ['leaves' => $leaves]);
}

    public function admEditLeave($id){
        $leaves = Leaves::where('id', $id)->first();

        return view('adm.admEditLeave', ['leave' => $leaves]);
    }

    public function admUpdateLeave(Request $request, $id){
        $leave = Leaves::find($id);

        if (!$leave) {
            return redirect()->back()->with('error', 'leave not found.');
        }

        // Validate form data
        $request->validate([
            'cid' => 'required|string',
            'name' => 'required|string',
            'start' => 'required|date',
            'end' => 'required|date',
            'remarks' => 'required|string',
            'image' => 'nullable|image|mimes:jpeg,png,jpg,gif|max:2048',
        ]);

        if ($request->hasFile('image')) {
            if ($leave->image) {
                Storage::disk('public')->delete('images/' . $leave->image);
            }

            $image = $request->file('image');
            $imageName = time() . '.' . $image->getClientOriginalExtension();
            $image->storeAs('images', $imageName, 'public');

            $leave->image = $imageName;
        }

        $leave->name = $request->name;
        $leave->cid = $request->cid;
        $leave->start = $request->start;
        $leave->end= $request->end;
        $leave->remarks = $request->remarks;

        $leave->save();

        return redirect()->route('getLeaveList')->with('success', 'Leave updated successfully.');
    }

    // Chart
    public function getDoctorsOnLeave(Request $request)
{
    $user = Auth::user();
    $month = $request->input('month');
    $leaveType = $request->input('leaveType');

    $doctorsOnLeave = DB::table('leaves')
        ->join('doctors', 'leaves.cid', '=', 'doctors.cid')
        ->whereMonth('start', $month)
        ->where('type', $leaveType)
        ->where('leaves.location', $user->location)
        ->where('leaves.hospitalName', $user->hospitalName)
        ->select('doctors.name', 'doctors.cid', 'doctors.specialization')
        ->get();

    return response()->json($doctorsOnLeave);
}



    public function admDeleteLeave($id){
        $leaves = Leaves::where('id', $id)->first();

        if ($leaves) {
            $leaves->delete();
            return redirect()->back()->with('success', 'Leave cancel successfully.');
        } else {
            return redirect()->back()->with('error', 'Leave not found.');
        }
    }

}
